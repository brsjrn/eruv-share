<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('admin'),
	'Create',
);\n";
?>

$this->menu=array(
	array('label'=>'Manage <?php echo $this->modelClass; ?>', 'url'=>array('admin')),
);
?>

<div id="top_admin_model">
	<h1>Create <?php echo $this->modelClass; ?><span class="back_admin"><?php echo "<?php echo CHtml::link('back', array('$this->modelClass/admin')); ?>"; ?></span></h1>

	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">create</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<?php 
	$isOrder = false;
	foreach($this->tableSchema->columns as $key=>$column) {
		if($column->name == 'ordre' ) {
			$isOrder = true;
		}
	}
?>

<?php
	if($isOrder) {
		echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model, 'listOrders' => \$listOrders)); ?>"; 
	} else {
		echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; 
	}
?>
</div>