<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$nameColumn=$this->guessNameColumn($this->tableSchema->columns);
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('admin'),
	\$model->{$nameColumn}=>array('view','id'=>\$model->{$this->tableSchema->primaryKey}),
	'Update',
);\n";
?>
?>

<div id="top_admin_model">
	<h1>Update <?php echo $this->modelClass." #<?php echo \$model->{$this->tableSchema->primaryKey}; ?>"; ?><span class="back_admin"><?php echo "<?php echo CHtml::link('back', array('$this->modelClass/admin')); ?>"; ?></span></h1>
	
	<div id="btn_model_area">
		<div class="btn_model btn_save_model label label-success">save</div>
		<?php echo "<?php echo CHtml::link('', array('$this->modelClass/view', 'id'=>\$model->id), array('class'=>'btn_model glyphicon glyphicon-eye-open')); ?>"; ?>
		<?php echo "<?php echo CHtml::link('', '#', array('submit'=>array('delete', 'id'=>\$model->id), 'confirm'=>'Are you sure you want to delete this item ?', 'class'=>'btn_model glyphicon glyphicon-trash')); ?>"; ?>
		<?php echo "<?php echo CHtml::link('', array('$this->modelClass/create'), array('class'=>'btn_model glyphicon glyphicon-plus')); ?>"; ?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div id="bottom_shadow"></div>

<div id="content_admin_model">
<?php 
	$isOrder = false;
	foreach($this->tableSchema->columns as $key=>$column) {
		if($column->name == 'ordre' ) {
			$isOrder = true;
		}
	}
?>

<?php
	if($isOrder) {
		echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model, 'listOrders' => \$listOrders)); ?>"; 
	} else {
		echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model)); ?>"; 
	}
?>
</div>