<?php
/**
 * This is the template for generating a controller class file for CRUD feature.
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>

class <?php echo $this->controllerClass; ?> extends <?php echo $this->baseControllerClass."\n"; ?>
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new <?php echo $this->modelClass; ?>;

		<?php 
			$isOrder = false;
			foreach($this->tableSchema->columns as $key=>$column) {
				if($column->name == 'ordre' ) {
					$isOrder = true;
					echo "
						\$listOrders = \$this->getListOrderCreate();
					";
				}
			}
		?>

		<?php
		$exist = false;
		foreach($this->tableSchema->columns as $key=>$column) {
			if(strpos($column->name, 'text') !== FALSE && !$exist) {
				$exist = true;
				echo "
					// KCFinder config
	                \$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
	                \$_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
	                \$_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				";
			}
		}
		?>

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];

			// Manage order
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if($column->name == 'ordre' ) {
						echo "
							if(\$model->ordre <= \$this->countModels()) {
				                // On récupère toutes les rubriques dont l'ordre est >=
				                \$listModels = \$this->getModelsSupOrdre(\$model->ordre);
				                
				                foreach (\$listModels as \$oldModel) {
				                    \$oldModel->ordre += 1;
				                    \$oldModel->save();
				                }
				            }
						";
					}
				}
			?>

			// Manage links
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if(strpos($column->name, 'link') !== FALSE ) {
						echo "
							if(strpos(\$model->$column->name, 'http') === FALSE && \$model->$column->name != '') {
				                \$model->$column->name = 'http://'. \$model->$column->name;
				            }
						";
					}
				}
			?>
			

			// Manage images upload
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if(strpos($column->name, 'image') !== FALSE ) {
						echo "
							\$tmp_".$key." = CUploadedFile::getInstance(\$model, '$column->name');

							if(isset(\$tmp_".$key.")) {
	                            \$model->$column->name = '$this->modelClass'.'_$key '. uniqid() .'.'. \$tmp_".$key."->extensionName;
	                        }
						";
					}
				}
			?>
            
			if($model->save()) {
				<?php 
					foreach($this->tableSchema->columns as $key=>$column) {
						if(strpos($column->name, 'image') !== FALSE ) {
							echo "
								if(isset(\$tmp_".$key.")) {
									if(!\$tmp_".$key."->saveAs('../images/$this->modelClass/'. \$model->$column->name)) {
                                         throw new ExceptionClass('Problem $this->modelClass image upload');
                                     }
								}
							";
						}
					}
				?>
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
			}
		}

		<?php
			if($isOrder) {
				echo  "
					\$this->render('create',array(
						'model' => \$model,
						'listOrders' => \$listOrders
					));
				";
			} else {
				echo  "
					\$this->render('create',array(
						'model' => \$model,
					));
				";
			}
		?>
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		<?php 
			$isOrder = false;
			foreach($this->tableSchema->columns as $key=>$column) {
				if($column->name == 'ordre' ) {
					$isOrder = true;
					echo "
						\$listOrders = \$this->getListOrderUpdate();
						\$oldOrdre = \$model->ordre;
					";
				}
			}
		?>

		<?php
		$exist = false;
		foreach($this->tableSchema->columns as $key=>$column) {
			if(strpos($column->name, 'text') !== FALSE && !$exist) {
				$exist = true;
				echo "
					// KCFinder config
	                \$_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
	                \$_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl.'/images/uploads/'; // URL for the uploads folder
	                \$_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath.'/../images/uploads/'; // path to the uploads folder

				";
			}
		}
		?>

		// Current images
		<?php 
			foreach($this->tableSchema->columns as $key=>$column) {
				if(strpos($column->name, 'image') !== FALSE ) {
					echo "
						\$old_". $key ." = \$model->$column->name;
					";	
				}
			}
		?>

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['<?php echo $this->modelClass; ?>']))
		{
			$model->attributes=$_POST['<?php echo $this->modelClass; ?>'];

			// Manage order
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if($column->name == 'ordre' ) {
						echo "
							// Si le nouveau rank est supérieur
	                        if(\$model->ordre > \$oldOrdre) {
	                            // On récupère toutes les rubriquesdont l'ordre est compris entre oldOrdre et model->ordre
	                            \$listModels = \$this->getModelsBetweenSupOrdres(\$oldOrdre, \$model->ordre);
	                            
	                            for(\$i=0; \$i<\$model->ordre-\$oldOrdre; \$i++) {
	                                \$oldModel = \$listModels[\$i];
	                                \$oldModel->ordre -= 1;
	                                \$oldModel->save();
	                            }
	                        }
	                        // Si le nouveau rank est inférieur
	                        if(\$model->ordre < \$oldOrdre) {
	                            // On récupère toutes les rubiques dont l'ordre est compris entre model->ordre et oldOrdre
	                            \$listModels = \$this->getModelsBetweenInfOrdres(\$model->ordre, \$oldOrdre);
	                            
	                            for(\$i=0; \$i<\$oldOrdre-\$model->ordre; \$i++) {
	                                \$oldModel = \$listModels[\$i];
	                                \$oldModel->ordre += 1;
	                                \$oldModel->save();
	                            }
	                        }
						";
					}
				}
			?>

			// Manage links
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if(strpos($column->name, 'link') !== FALSE ) {
						echo "
							if(strpos(\$model->$column->name, 'http') === FALSE && \$model->$column->name != '') {
				                \$model->$column->name = 'http://'. \$model->$column->name;
				            }
						";
					}
				}
			?>

			// Manage images upload
			<?php 
				foreach($this->tableSchema->columns as $key=>$column) {
					if(strpos($column->name, 'image') !== FALSE ) {
						echo "
							\$tmp_".$key." = CUploadedFile::getInstance(\$model, '$column->name');

							if(isset(\$tmp_".$key.")) {
	                            \$model->$column->name = '$this->modelClass'.'_$key'. uniqid() .'.'. \$tmp_".$key."->extensionName;
	                        }
	                        if(!isset(\$tmp_".$key.") && isset(\$old_". $key .")) {
	                            \$model->$column->name = \$old_". $key .";
	                        }
						";
					}
				}
			?>

			if($model->save()) {
				<?php 
					foreach($this->tableSchema->columns as $key=>$column) {
						if(strpos($column->name, 'image') !== FALSE ) {
							echo "
								if(isset(\$tmp_".$key.")) {
									if(!\$tmp_".$key."->saveAs('../images/$this->modelClass/'. \$model->$column->name)) {
                                         throw new ExceptionClass('Problem $this->modelClass image upload');
                                     }
                                     if(isset(\$old_". $key .") && strlen(\$old_". $key .")>0) {
	                                    unlink('../images/$this->modelClass/' . \$old_". $key .");
	                                }
								}
							";
						}
					}
				?>
				$this->redirect(array('view','id'=>$model-><?php echo $this->tableSchema->primaryKey; ?>));
			}
		}

		<?php
			if($isOrder) {
				echo  "
					\$this->render('update',array(
						'model' => \$model,
						'listOrders' => \$listOrders
					));
				";
			} else {
				echo  "
					\$this->render('update',array(
						'model' => \$model,
					));
				";
			}
		?>
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
		$model->delete();

		 <?php
	        foreach($this->tableSchema->columns as $key=>$column) {
				if($column->name == 'ordre' ) {
					echo "
						\$nbModels = \$this->countModels()+1;

						// On récupère toutes les rubriques dont l'ordre est compris entre oldOrdre et model->ordre
				        \$listModels = \$this->getModelsBetweenSupOrdres(\$model->ordre, \$nbModels);

				        for(\$i=0; \$i<\$nbModels-\$model->ordre; \$i++) {
				            \$oldModel = \$listModels[\$i];
				            \$oldModel->ordre -= 1;
				            \$oldModel->save();
				        }
					";
				}
			}
		?>

		
                
        <?php
	        foreach($this->tableSchema->columns as $key=>$column) {
				if(strpos($column->name, 'image') !== FALSE ) {
					echo "
						if(isset(\$model->$column->name) && \$model->$column->name != '') {
				            unlink('../images/$this->modelClass/'. \$model->$column->name);
				        }
					";
				}
			}
		?>
        

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('<?php echo $this->modelClass; ?>');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new <?php echo $this->modelClass; ?>('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['<?php echo $this->modelClass; ?>']))
			$model->attributes=$_GET['<?php echo $this->modelClass; ?>'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return <?php echo $this->modelClass; ?> the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=<?php echo $this->modelClass; ?>::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param <?php echo $this->modelClass; ?> $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='<?php echo $this->class2id($this->modelClass); ?>-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


    /**
     * Get nb model instances
     */
    protected function countModels() {
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll();
        
        return count($listModels);
    }

    /**
     * Get model with order superiori to order param
     */
    protected function getModelsSupOrdre($ordre) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordre');
        $criteria->params = array(
                ':ordre'=>$ordre
        );
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll($criteria);
        
        return $listModels;
    }

     /**
     * Get order list to create model
     */
    protected function getListOrderCreate() {
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels)+1; $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Donne la liste des ordres à afficher dans le formulaire d'update
     */
    protected function getListOrderUpdate() {
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll();

        $listOrders = array();
        for($i=1; $i<=count($listModels); $i++) {
            $listOrders[$i] = $i;
        }
        
        return $listOrders;
    }


    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant remonté)
     */
    protected function getModelsBetweenSupOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>:ordreDeb');
        $criteria->addCondition('ordre<=:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll($criteria);
        
        return $listModels;
    }

    /**
     * Récupère Video pour un véhicule donné, dont l'ordre est entre ordreDeb et ordreFin (ordre courant redescendu)
     */
    protected function getModelsBetweenInfOrdres($ordreDeb, $ordreFin) {
        $criteria = new CDbCriteria();
        $criteria->addCondition('ordre>=:ordreDeb');
        $criteria->addCondition('ordre<:ordreFin');
        $criteria->params = array(
                ':ordreDeb'=>$ordreDeb,
                ':ordreFin'=>$ordreFin
        );
        $listModels = <?php echo $this->modelClass; ?>::model()->findAll($criteria);
        
        return $listModels;
    }
}
