'use strict';

// Var global
var win;

// Var sizes
var header,
    nav,
    btnMenuResponisve,
    menuContentResponsive;

// Testimonial slider
var testiCpt = 0,
    testiTotal = 0;

$(document).ready(function(){
    // Init
    initVariables();
    resizeBrowser();

    // Resize window
    $(window).resize(function() {
        resizeBrowser();
    });

    // Scroll manager
    win.on("scroll", function() {

        // Change menu depend on scroll position
        if($(document).scrollTop() >= 300) { // win.height() - 30
            nav.addClass('white');
        } else {
            nav.removeClass('white');
        }
    });

    // Call functions
    //new $.defilement();

    // Responsive menu
    btnMenuResponisve.on('click', function() {
        if($(this).hasClass('open')) {
            $(this).removeClass('open');
            menuContentResponsive.slideUp();
        } else {
            $(this).addClass('open');
            menuContentResponsive.slideDown();
        }
    })

    // Scroll to
    $(".product-link").click(function() {
        var linkClicked = $(this);

        if(linkClicked.hasClass('link-responsive')) {
            btnMenuResponisve.removeClass('open');
            menuContentResponsive.slideUp();
        }

        $('html, body').animate({
            scrollTop: $("#part-1").offset().top
        }, 1000);
    });

    $(".testimonials-link").click(function() {
        var linkClicked = $(this);

        if(linkClicked.hasClass('link-responsive')) {
            btnMenuResponisve.removeClass('open');
            menuContentResponsive.slideUp();
        };

        $('html, body').animate({
            scrollTop: $("#part-5").offset().top
        }, 1000);
    });

    $(".menches-link").click(function() {
        var linkClicked = $(this);

        if(linkClicked.hasClass('link-responsive')) {
            btnMenuResponisve.removeClass('open');
            menuContentResponsive.slideUp();
        }

        $('html, body').animate({
            scrollTop: $("#part-9").offset().top
        }, 1000);
    });

    $(".top-link").click(function() {
        var linkClicked = $(this);

        if(linkClicked.hasClass('link-responsive')) {
            btnMenuResponisve.removeClass('open');
            menuContentResponsive.slideUp();
        }

        $('html, body').animate({
            scrollTop: $("#header").offset().top
        }, 1000);
    });

    // Swipe testimonial
    $("#part-5").on("swipeleft",function(){
      var oldElt = $('#quotes .elt').eq(testiCpt);

      if(testiCpt+1 < testiTotal) {
          testiCpt++;
      } else {
          testiCpt = 0;
      }

      var newElt = $('#quotes .elt').eq(testiCpt);

      animSlider(oldElt, newElt);
    });

    $("#part-5").on("swiperight",function(){
      var oldElt = $('#quotes .elt').eq(testiCpt);

      if(testiCpt > 0) {
          testiCpt--;
      } else {
          testiCpt = testiTotal-1;
      }

      var newElt = $('#quotes .elt').eq(testiCpt);

      animSlider(oldElt, newElt);
    });


    // Manage testimonial slider
    // Init
    $('#quotes .elt').hide();
    $('#quotes .elt').eq(0).show();

    // Click right
    $('.nav-right').on('click', function() {
        var oldElt = $('#quotes .elt').eq(testiCpt);

        if(testiCpt+1 < testiTotal) {
            testiCpt++;
        } else {
            testiCpt = 0;
        }

        var newElt = $('#quotes .elt').eq(testiCpt);

        animSlider(oldElt, newElt);
    });

    // Click left
    $('.nav-left').on('click', function() {
        var oldElt = $('#quotes .elt').eq(testiCpt);

        if(testiCpt > 0) {
            testiCpt--;
        } else {
            testiCpt = testiTotal-1;
        }

        var newElt = $('#quotes .elt').eq(testiCpt);

        animSlider(oldElt, newElt);
    });
});

function initVariables() {
    win = $(window);
    header = $('#header');
    nav = $('#nav');
    btnMenuResponisve = $('#btn-menu-responisve');
    menuContentResponsive = $('#menu-content-responsive');

    testiTotal = $('#quotes .elt').length;
}

function resizeBrowser() {
    //header.height(win.height() - nav.height());

    // Resize dynamic responsive
    if(viewport().width <= 1080) {

    } else {

    }

    // Resize full-images
    $('.full-image').each(function() {
        var parent = $(this).parent('.full-image-parent');
        adaptImageToResolution(parent, $(this))
    });

}

/* ----------------------- */
/*          TOOLS          */
/* ----------------------- */
// Get viewport dimensions
function viewport() {
    var e = window, a = 'inner';
    if(!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return { width : e[ a+'Width' ] , height : e[ a+'Height' ]};
}

/* adapte project detail cover to resolution */
function adaptImageToResolution(ref, target) {
    var newX;
    var newY;
    var deltaX;
    var deltaY;
    var deltaMax;
    var moveX;
    var moveY;

    var refWidth = ref.width();
    var targetWidth = target.width();
    deltaX = refWidth/targetWidth;
    deltaY = ref.height()/target.height();

    deltaMax = Math.max(deltaX, deltaY);

    newX = target.width()*deltaMax;
    newY = target.height()*deltaMax;

    moveX = (newX - ref.width())/2;
    moveY = (newY - ref.height())/2;

    target.width(newX);
    target.height(newY);
    target.css("margin-left", '-'+moveX+'px');
    target.css("margin-top", '-'+moveY+'px');
}

/* Slider testimnoial */
function animSlider(oldElt, newElt) {
    oldElt.fadeOut(function() {
        newElt.fadeIn();
    })
}