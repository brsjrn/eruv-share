(function ($) {
	$.defilement = function(element, options) {
		var _this = this;

        _this.defaults = {
        };

		_this.init = function() {
            _this.options = $.extend(_this.defaults, options);

            // Global
            _this.totalElt = $('#team-content-1-portraits .team-content-portraits-image').length;
            _this.currentElt = 0;
            _this.totalElt_responsive = $('#team-content-responsive .team-content-responsive-portraits-image').length;
            _this.currentElt_responsive = 0;

            // Elements
            _this.$teamContentPortraits_1 = $('#team-content-1-portraits');
            _this.$teamContentPortraits_2 = $('#team-content-2-portraits');


			_this.build();


			// Init list tmp elts
			/*
			for (var i = 0; i < _this.$listElts.length; i++) {
				_this.listTmpElts[i] = _this.$listElts.eq(i);
			};
			*/
		};

		_this.build = function() {
			console.log("Total elts : "+ _this.totalElt);

			$('#team-content-navigation-arrow-top').on('click', function() {
				_this.next();
			});

			$('#team-content-navigation-arrow-bottom').on('click', function() {
				_this.prev();
			});
			$('#team-content-responsive-navigation-arrow-right').on('click', function() {
				_this.next_responsive();
			});

			$('#team-content-responsive-navigation-arrow-left').on('click', function() {
				_this.prev_responsive();
			})
		};

		_this.next = function() {
			console.log('next');

			var oldEltIndex = _this.currentElt;

			if(_this.currentElt < _this.totalElt - 1) {
				_this.currentElt++;
			} else {
				_this.currentElt = 0;
			}

			var oldElt1 = _this.$teamContentPortraits_1.children('.team-content-portraits-image').eq(oldEltIndex),
				currentElt1 = _this.$teamContentPortraits_1.children('.team-content-portraits-image').eq(_this.currentElt),
				oldElt2 = _this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(oldEltIndex),
				currentElt2 = _this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(_this.currentElt);

			oldElt1.css('z-index', 3);
			currentElt1.css('z-index', 2);
			oldElt2.css('z-index', 3);
			currentElt2.css('z-index', 2);

			oldElt1.animate({height: 0}, function() {
				oldElt1.css('height', '100%');
				oldElt1.css('z-index', 1);
				currentElt1.css('z-index', 2);
			});

			oldElt2.animate({height: 0}, function() {
				oldElt2.css('height', '100%');
				oldElt2.css('z-index', 1);
				currentElt2.css('z-index', 2);
			});

			//_this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(oldElt).css('z-index', 1);
			//_this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(_this.currentElt).css('z-index', 2);

			// Scroll text
			$('#team-content-1-text').children('.team-content-text-elt').eq(_this.currentElt).css('top', $('#team-content-1-text').height());
			$('#team-content-1-text').children('.team-content-text-elt').eq(_this.currentElt).animate({top: 0});
			$('#team-content-1-text').children('.team-content-text-elt').eq(oldEltIndex).animate({top: -$('#team-content-1-text').height()});

			$('#team-content-2-text').children('.team-content-text-elt').eq(_this.currentElt).css('top', $('#team-content-1-text').height());
			$('#team-content-2-text').children('.team-content-text-elt').eq(_this.currentElt).animate({top: 0});
			$('#team-content-2-text').children('.team-content-text-elt').eq(oldEltIndex).animate({top: -$('#team-content-1-text').height()});
		
			// Pagination animation
			$('.first-number').children('.number').eq(_this.currentElt).css('top', $('.first-number').height());
			$('.first-number').children('.number').eq(oldEltIndex).animate({top: -15});
			$('.first-number').children('.number').eq(_this.currentElt).animate({top: 0});
		}

		_this.prev = function() {
			console.log('prev');

			var oldEltIndex = _this.currentElt;

			if(_this.currentElt > 0) {
				_this.currentElt--;
			} else {
				_this.currentElt = _this.totalElt - 1;
			}

			var oldElt1 = _this.$teamContentPortraits_1.children('.team-content-portraits-image').eq(oldEltIndex),
				currentElt1 = _this.$teamContentPortraits_1.children('.team-content-portraits-image').eq(_this.currentElt),
				oldElt2 = _this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(oldEltIndex),
				currentElt2 = _this.$teamContentPortraits_2.children('.team-content-portraits-image').eq(_this.currentElt);

			oldElt1.css('z-index', 3);
			currentElt1.css('z-index', 2);
			oldElt2.css('z-index', 3);
			currentElt2.css('z-index', 2);

			oldElt1.animate({height: 0}, function() {
				oldElt1.css('height', '100%');
				oldElt1.css('z-index', 1);
				currentElt1.css('z-index', 2);
			});

			oldElt2.animate({height: 0}, function() {
				oldElt2.css('height', '100%');
				oldElt2.css('z-index', 1);
				currentElt2.css('z-index', 2);
			});

			// Scroll text
			$('#team-content-1-text').children('.team-content-text-elt').eq(_this.currentElt).css('top', -$('#team-content-1-text').height());
			$('#team-content-1-text').children('.team-content-text-elt').eq(_this.currentElt).animate({top: 0});
			$('#team-content-1-text').children('.team-content-text-elt').eq(oldEltIndex).animate({top: $('#team-content-1-text').height()});

			$('#team-content-2-text').children('.team-content-text-elt').eq(_this.currentElt).css('top', -$('#team-content-1-text').height());
			$('#team-content-2-text').children('.team-content-text-elt').eq(_this.currentElt).animate({top: 0});
			$('#team-content-2-text').children('.team-content-text-elt').eq(oldEltIndex).animate({top: $('#team-content-1-text').height()});
		
			// Pagination animation
			$('.first-number').children('.number').eq(_this.currentElt).css('top', -$('.first-number').height());
			$('.first-number').children('.number').eq(oldEltIndex).animate({top: $('.first-number').height()});
			$('.first-number').children('.number').eq(_this.currentElt).animate({top: 0});
		}

		_this.next_responsive = function() {
			console.log('next');

			var oldEltIndex = _this.currentElt_responsive;

			if(_this.currentElt_responsive < _this.totalElt_responsive - 1) {
				_this.currentElt_responsive++;
			} else {
				_this.currentElt_responsive = 0;
			}

			var oldElt = $('.team-content-responsive-portraits-image').eq(oldEltIndex),
				currentElt = $('.team-content-responsive-portraits-image').eq(_this.currentElt_responsive);
				
			oldElt.css('z-index', 3);
			currentElt.css('z-index', 2);

			oldElt.animate({height: 0}, function() {
				oldElt.css('height', '100%');
				oldElt.css('z-index', 1);
				currentElt.css('z-index', 2);
			});

			// Scroll text
			$('.team-content-responsive-text-elt').eq(_this.currentElt_responsive).css('top', $('#team-content-responsive-text').height());
			$('.team-content-responsive-text-elt').eq(_this.currentElt_responsive).animate({top: 0});
			$('.team-content-responsive-text-elt').eq(oldEltIndex).animate({top: -$('#team-content-responsive-text').height()});
		}

		_this.prev_responsive = function() {
			console.log('prev');

			var oldEltIndex = _this.currentElt_responsive;

			if(_this.currentElt_responsive > 0) {
				_this.currentElt_responsive--;
			} else {
				_this.currentElt_responsive = _this.totalElt_responsive - 1;
			}

			var oldElt = $('.team-content-responsive-portraits-image').eq(oldEltIndex),
				currentElt = $('.team-content-responsive-portraits-image').eq(_this.currentElt_responsive);
				
			oldElt.css('z-index', 3);
			currentElt.css('z-index', 2);

			oldElt.animate({height: 0}, function() {
				oldElt.css('height', '100%');
				oldElt.css('z-index', 1);
				currentElt.css('z-index', 2);
			});

			// Scroll text
			$('.team-content-responsive-text-elt').eq(_this.currentElt_responsive).css('top', -$('#team-content-responsive-text').height());
			$('.team-content-responsive-text-elt').eq(_this.currentElt_responsive).animate({top: 0});
			$('.team-content-responsive-text-elt').eq(oldEltIndex).animate({top: $('#team-content-responsive-text').height()});
		}

		_this.viewport = function() {
		    var e = window, a = 'inner';
		    if(!('innerWidth' in window)) {
		        a = 'client';
		        e = document.documentElement || document.body;
		    }
		    return { width : e[ a+'Width' ] , height : e[ a+'Height' ]};
		}


		_this.init();
	};
})(jQuery);