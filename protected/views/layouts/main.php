<?php 
    /* @var $this Controller */ 
    $baseUrl = Yii::app()->baseUrl; 
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
        <meta name="viewport" content="width=device-width, maximum-scale=1, initial-scale=1, minimum-scale=1">
        <meta name="format-detection" content="telephone=no">
            
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<!-- Css -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/vendor/uikit/css/uikit.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
    
        <?php 
                /* Load jQuery & JS files */
                Yii::app()->clientScript->registerCoreScript('jquery');
                //$cs = Yii::app()->getClientScript();
                //$cs->registerScriptFile($baseUrl.'/js/global.js');
        ?>
            
        <?php echo $content; ?>
	
         <!-- Javascript -->
         <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/lib/jquery.mobile.custom.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/vendor/uikit/js/uikit.min.js"></script>
    <script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/js/global.js"></script>

</body>
</html>
