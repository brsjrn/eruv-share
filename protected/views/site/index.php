<?php 
    /* @var $this Controller */ 
    $baseUrl = Yii::app()->baseUrl; 
?>


<header id="header" class="full-image-parent" data-uk-scrollspy="{cls:'uk-animation-fade'}">

	<!--<img src="<?php echo $baseUrl .'/images/header-bg2.jpg' ?>" class="full-image desktop" width="2880" height="1645" /> -->
	<div id="uikitex-parallax" style="background-image:url(images/header-bg3.jpg);" class="desktop uk-cover-background" ></div>
	<div id="uikitex-parallax-responsive" style="background-image:url(images/header-bg-responsive3.jpg);" class="responsive uk-cover-background" ></div>

	<div id="header-content" class="xlarge-width">
		<div id="header-content-inner">
			<div id="title">
				<span class="desktop">Belonging is more<br />than just joining.</span>
				<span class="responsive">Belonging is more than just joining.</span>
			</div>
			<div id="subtitle">Member engagement for the modern american temple. </div>

			<div id="header-buttons">
				<a href="#" id="btn-video" class="btn-animate ">
					<div id="btn-video-picto"></div>
					<div id="btn-video-text">
						<div class="btn-content-hover">Play video</div>
						<div class="btn-content">Play video</div>
					</div>
					<div class="clear"></div>
				</a>
				<!--
				<a href="#" id="link-dl-sheet">
					or download fact sheet
					<div id="lisert-dl-sheet"></div>
				</a>
				-->
				<div class="clear"></div>
			</div>
		</div>
	</div>
</header>

<nav id="nav">
	<div id="nav-content" class="xlarge-width desktop"> 
		<!-- Menu -->
	    <ul id="menu">
	        <li class="product-link menu-link">Product</li>
	        <li class="testimonials-link menu-link">Testimonials</li>
	        <li class="menches-link menu-link">Your Mensches</li>
	    </ul>

	    <!-- Buttons -->
    	<a href="mailto:hello@eruv.com" id="nav-email">
    		hello@eruv.com
			<div id="nav-email-liseret" class="liseret-anim"></div>
		</a>
	    <a href="#" id="btn-sign-up" class="button button-white btn-animate ">
	    	<div class="btn-content-hover">Sign in</div>
	    	<div class="btn-content">Sign in</div>
    	</a>

	    <div class="clear"></div>

	    <!-- Logo -->
	    <img src="<?php echo $baseUrl .'/images/logo-white.png' ?>" id="logo-nav-white" class="logo-nav top-link " width="109" height="35" />
	    <img src="<?php echo $baseUrl .'/images/logo-black.png' ?>" id="logo-nav-black" class="logo-nav top-link" width="109" height="35" width="" height="" />
    </div>

    <img src="<?php echo $baseUrl .'/images/logo-responsive.jpg' ?>" id="logo-nav-responsive" class="responsive top-link link-responsive" width="89" height="28" width="" height="" />

    <div id="btn-menu-responisve" class="menu-lines-button responsive">
    	<div class="line line-1"></div>
    	<div class="line line-2"></div>
    	<div class="line line-3"></div>
    </div>

    <div id="menu-content-responsive" class="responsive">
    	<div id="menu-responisve-shadow"></div>

    	<ul id="menu-responsive">
    	    <li class="product-link link-responsive">Product</li>
    	    <li class="testimonials-link link-responsive">Testimonials</li>
    	    <li class="menches-link link-responsive">Your Mensches</li>
    	    <li>Sign in</li>
    	</ul>
    </div>
</nav>

<div id="part-1" class="part" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false ,delay:100}">
	<div id="part-1-content" class="xsmall-width">
		<h2>Technology cannot replace community.</h2>
		<div class="text text-1">Your Eruv, with your brand and your information, is the private extension of your Jewish community. It lets your members interact anywhere, anytime giving you insights about what’s important to them. Because modern communities do not thrive without technology, your Eruv keeps your members connected even when they are not at temple.</div>
	</div>

	<div id="part-1-content-2" class="xsmall-width">
		<div id="text-1">er•uv  /ˈero͝ov/</div>
		<!--<div id="text-2">(<b>air</b>-oo v, <b>er</b>-; Sephardic Hebrew e-roov; Ashkenazic Hebrew ey-<b>roo</b> v)</div>-->
		<div id="text-3">An urban area enclosed by a wire boundary that symbolically extends the private domain of Jewish households into public areas.  We take “eruv,” as simply an extension of your congregation that your members keep with them wherever they go.</div>
	</div>
</div>

<div id="arrow-go-bottom" class="desktop">
	<div id="liseret"></div>
	<div id="btn"></div>
</div>

<div id="part-2" class="part">
	<img src="<?php echo $baseUrl .'/images/part-2-laptop-responisve.jpg' ?>" class="responsive" alt="iPhone" width="100%" height="auto" />

	<img src="<?php echo $baseUrl .'/images/part-2-laptop.jpg' ?>" id="part-2-laptop" class="desktop" alt="iPhone" width="591" height="460" data-uk-scrollspy="{cls:'uk-animation-slide-right', topoffset: -100, repeat:false, delay:100}" />

	<img src="<?php echo $baseUrl .'/images/part-2-pen.jpg' ?>" id="part-2-pen" class="desktop" alt="iPhone" width="133" height="216" data-uk-scrollspy="{cls:'uk-animation-slide-left', topoffset: -100, repeat:false, delay:100}" />

	<div id="part-2-content" class="medium-width" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false, delay:300}">
	<!-- <div id="part-2-content" class="large-width"> -->
		<div id="part-2-left">
			<div id="part-2-left-content-1">
				<!-- <div id="subtitle" class="responsive">Engage with your community :</div> -->
				<h2 class="desktop">
				Your custom-branded<br />
				Eruv web-app can help<br />
				build your community<br />
				one device at a time.
				</h2>
				<h2 class="responsive">Your custom-branded Eruv web-app on your member’s devices.</h2>
				<div class="text text-1">
					<ul>
						<li>Allow members to find business connections within the congregation</li>
						<li>Let parents know what their kids are learning</li>
						<li>Help new members connect</li>
						<li>Empower groups with their own photo albums, discussions, and event calendars</li>
						<li>Meet people like you</li>
						<li>Know how to reach out to each member</li>
						<li>Know who else will be at an event before you attend</li>
						<li>Take part in discussions even when you can't be there</li>
					</ul>
				</div>
			</div>

			<div id="part-2-left-content-2">
				<div id="quote">“The big thing is that members want to go there every day as<br />opposed to it being one more bit of time-consuming tech in their day.</div>
				<div id="signature">Executive Director, Chicago, IL.</div>
			</div>
		</div>

		<!--
		<div id="part-2-right" class="desktop">
			<img src="<?php echo $baseUrl .'/images/part-2-laptop.jpg' ?>" alt="iPhone" width="100%" height="auto" />
		</div>
		-->

		<div class="clear"></div>
	</div>
</div>

<div id="part-3" class="part">

	<img src="<?php echo $baseUrl .'/images/part-3-ecouteurs.jpg' ?>" id="part-3-ecouteurs" class="desktop" alt="laptop" width="260" height="507" data-uk-scrollspy="{cls:'uk-animation-slide-right', topoffset: -100, repeat:false, delay:100}" />

	<img src="<?php echo $baseUrl .'/images/part-3-book.jpg' ?>" id="part-3-book" class="desktop" alt="laptop" width="284" height="623" data-uk-scrollspy="{cls:'uk-animation-slide-left', topoffset: -100, repeat:false, delay:100}" />

	<div id="part-3-responsive-images" class="responsive">
		<img src="<?php echo $baseUrl .'/images/part-3-iphone-responsive.jpg' ?>" width="100%" height="auto" />
	</div>

	<div id="part-3-content" class="medium-width" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false, delay:300}">
		<div id="part-3-left" class="desktop">
			<img src="<?php echo $baseUrl .'/images/part-3-iphone.jpg' ?>" alt="laptop" width="561" height="701" />
		</div>
		<div id="part-3-right">
			<div id="part-3-left-content">
				<div id="support-choice">
					<img src="<?php echo $baseUrl .'/images/laptop-choice.png' ?>" class="support" id="laptop" alt="laptop" width="38%" height="auto" />
					<img src="<?php echo $baseUrl .'/images/desktop-choice.png' ?>" alt="desktop" class="support" id="desktop" width="34%" height="auto" />
					<img src="<?php echo $baseUrl .'/images/mobile-choice.png' ?>" alt="mobile" class="support" id="mobile" width="10%" height="auto" />
					<div class="clear"></div>
				</div> 

				<h2>Each member’s personalized view into your community.</h2>
				<div class="text text-1">
					<div class="desktop">
						<p>Members see discussions, photos, events,<br />and other families personalized to their interests.</p>
						<p>Your Eruv targets each member<br />with only relevant notifications. </p>
					</div>
					<div class="responsive">
						<p>Members see discussions, photos, events, and other families personalized to their interests. Your Eruv targets each member with only relevant notifications.</p>
					</div>
				</div>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</div>

<!-- <div id="liseret-separation" class="large-width"></div> -->

<div id="part-4" class="part" data-uk-scrollspy="{cls:'uk-animation-fade', repeat:false, delay:100}">
	<div id="part-4-content" class="medium-width">
		<h2>How it Works</h2>
		<ul id="part-4-list">

			<div id="liseret" class="desktop"></div>

		    <li class="first list-elt">
		    	<img src="<?php echo $baseUrl .'/images/picto-1-hover.jpg' ?>" alt="members" width="65" height="50" />
		    	<div class="number">
		    		<div class="circle-1"></div>
		    		<div class="circle-2"></div>
		    		<div class="num">1</div>
		    	</div>
		    	<div class="text">
		    		<h3>Amplify leadership</h3>
		    		<div class="text-1">
		    			Leaders easily communicate the right information to the right people at the right time making members feel that they truly belong.
		    		</div>
		    	</div>
		    </li>
		    <li class="list-elt">
		    	<img src="<?php echo $baseUrl .'/images/picto-2-hover.jpg' ?>" alt="administrators" width="65" height="50" />
		    	<div class="number">
		    		<div class="circle-1"></div>
		    		<div class="circle-2"></div>
		    		<div class="num">2</div>
		    	</div>
		    	<div class="text">
		    		<h3>Engage members</h3>
		    		<div class="text-1">
		    			Members quickly share and learn about other members leading to real meaningful interactions within the community.
		    		</div>
		    	</div>
		    </li>
		    <li class="list-elt">
		    	<img src="<?php echo $baseUrl .'/images/picto-3-hover.jpg' ?>" alt="rest" width="65" height="50" />
		    	<div class="number">
		    		<div class="circle-1"></div>
		    		<div class="circle-2"></div>
		    		<div class="num">3</div>
		    	</div>
		    	<div class="text">
		    		<h3>We do the rest</h3>
		    		<div class="text-1">
		    			First-line support answering any technical questions; maintaining servers, upgrades, security patches, and all other IT overhead so you can focus on engaging your community.
		    		</div>
		    	</div>
		    </li>
		    <div class="clear"></div>
		</ul>
	</div>
</div>

<div id="part-5" class="part">
	<div id="part-5-content" class="small-width">

		<div id="part-5-nav-responsive" class="responsive">
			<img src="<?php echo $baseUrl .'/images/nav-left.png' ?>" class="nav-testimonial-responsive nav-left" width="13" height="23" />
			<img src="<?php echo $baseUrl .'/images/nav-right.png' ?>" class="nav-testimonial-responsive nav-right" width="13" height="23" />
		</div>

		<h3>Testimonials</h3>

		<div id="quotes">
			<div class="elt">
				<div class="quote">
					“There is no doubt in my mind that we have filled a need in our congregation.”
				</div>
				<div class="signature">
					Lay Leader, Richmond, VA
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					"I'm incredibly impressed with the thoughtfulness that went into this. Absolutely amazing!"
				</div>
				<div class="signature">
					Executive Director, Rockville, MD
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					“I love that the platform and the technology is on your side.  Their commitment to customer service is what expedites the learning curve.”<br />
					"Phenomenally overwhelmingly amazing. I can see this going in a million directions."
				</div>
				<div class="signature">
					Executive Director, Penn Valley, PA
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					"I'm 71 and I can't believe how easy this is"
				</div>
				<div class="signature">
					Lay Leader, Dallas TX
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					“This is a tool for breaking the paradigm.  Some people can be here and some can’t.  This helps us serve both of those populations.”<br />
					“It’s not something I have to spend a significant amount of time to manage.  Teachers and parents are the ones adding content.”
				</div>
				<div class="signature">
					Principal, San Diego, CA
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					“Preschool families ask us all the time for referrals to connect with other families. Now it happens automatically when they sign up!”
				</div>
				<div class="signature">
					 Pre-school Director, San Diego, CA
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					"Tremendous power behind this pretty page"
				</div>
				<div class="signature">
					Executive Director, Tarzana, CA
				</div>
			</div>
			<div class="elt">
				<div class="quote">
					"Everybody loved it. There wasn’t one person who said “ehh”"
				</div>
				<div class="signature">
					Executive Director, Birmingham, AL
				</div>
			</div>
		</div>

		<div class="nav-testimonial nav-left desktop">
			<img src="<?php echo $baseUrl .'/images/nav-left.png' ?>" width="13" height="22" />
			<div class="arrow-trait"></div>
		</div>
		<div class="nav-testimonial nav-right desktop">
			<img src="<?php echo $baseUrl .'/images/nav-right.png' ?>" width="13" height="22" />
			<div class="arrow-trait"></div>
		</div>

			<!--
		<img src="<?php echo $baseUrl .'/images/nav-left.png' ?>" class="nav-testimonial nav-left desktop" width="13" height="23" />
		<img src="<?php echo $baseUrl .'/images/nav-right.png' ?>" class="nav-testimonial nav-right desktop" width="13" height="23" />
		-->
	</div>
</div>

<!--
<div id="part-6" class="part">
	<div id="part-6-content" class="large-width">
		<div id="part-6-left">
			<div class="part-6-title">
				<div class="number">/1</div>
				<h3>How it works</h3>
			</div>
			<div class="text text-1">
				<ul>
				    <li>We guide you in customizing your site for your congregation. You brand the site with your name and logo and you select the questions your members are asked at sign up, customizing their entire experience.</li>
					<li>We train your staff and lay leaders helping to create relevant groups and creating confident experts on your eruv.</li>
					<li>We provide template marketing materials and engagement ideas to help you develop a deployment plan that works for your congregation.</li>
				</ul>
			</div>
		</div>

		<div id="part-6-right">
			<div class="part-6-title">
				<div class="number">/2</div>
				<h3>Off to the race</h3>
			</div>
			<div class="text text-1">
				<ul>
				    <li>We provide first-line support.  When one of your members submits a question, we answer it, forwarding relevant inquiries to your staff.</li>
					<li>We continue to provide support for you and your staff whenever questions arise.</li>
				</ul>
			</div>
		</div>

		<div class="clear"></div>
	</div>
</div>

<div id="liseret-separation" class="large-width"></div>
-->

<div id="part-7" class="part" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false ,delay:100}">
	<div id="part-7-content" class="large-width">
		<h2>Pricing</h2>
		<div class="text text-1 desktop">The cost for each eruv site depends on the total number of families in the congregation,<br />
		the amount of customization, and the level of support and training.<br />
		The average site costs about nine dollars per family per year.</div>
		<div class="text text-1 responsive">The cost for each eruv site depends on the total number of families in the congregation, the amount of customization, and the level of support and training. The average site costs about nine dollars per family per year.</div>
	</div>
</div>

<div id="part-8" class="part desktop">
	<div id="part-8-content" class="large-width">
		<img src="<?php echo $baseUrl .'/images/download-sheet.jpg' ?>" alt="download sheet" width="42" height="45" />
		<div id="download-text" class="desktop">Want to download the<br />
		Eruv fact sheet?</div>
		<div id="download-text" class="responsive">Want to download the Eruv fact sheet?</div>
		<a href="#" id="btn-download" class="button button-white btn-animate">
			<div class="btn-content-hover">Download</div>
			<div class="btn-content">Download</div>
		</a>
	</div>
</div>

<div id="part-9" class="part">
	<div id="part-9-content" class="large-width">
		<div id="part-9-story" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false ,delay:100}">
			<h2>The Story</h2>
			<div class="text text-1 desktop">
				<p>
					We created eruv because we didn't feel as connected as we wanted to be to our<br />
					own synagogues.  After we each started families, we realized that it was difficult to find<br />
					other families with similar interests and family make-ups.
				</p>
				<p>
					Combining our complementary expertise, we created Eruv.  Working closely with early adopting<br />congregations around the country, we refined our ideas with feedback and guidance from our customers<br />
					to organically grow eruv. Today eruv is still very much evolving based on customer driven improvements.
				</p>
			</div>
			<div class="text text-1 responsive">
				<p>
					We created eruv because we didn't feel as connected as we wanted to be to our own synagogues.  After we each started families, we realized that it was difficult to find other families with similar interests and family make-ups.
				</p>
				<p>
					Combining our complementary expertise, we created Eruv.  Working closely with early adopting congregations around the country, we refined our ideas with feedback and guidance from our customers to organically grow eruv. Today eruv is still very much evolving based on customer driven improvements..
				</p>
			</div>
		</div>

		<div id="part-9-persons" class="desktop">
			<div id="part-9-person-1" class="part-9-person">
				<div class="portrait" data-uk-scrollspy="{cls:'uk-animation-slide-left', topoffset: -100, repeat:false ,delay:100}">
					<div class="portrait-bg"></div>
					<img src="<?php echo $baseUrl .'/images/person-12.png' ?>" width="475" height="460" />
				</div>
				<div class="infos" data-uk-scrollspy="{cls:'uk-animation-slide-right', topoffset: -100, repeat:false ,delay:100}">
					<div class="infos-content">
						<div class="name">Adam Furman</div>
						<div class="status">Founder & CEO</div>
						<ul class="text-1">
						    <li>MBA, Rady School of Management, UCSD</li>
						    <li>Leadership of Global IT Operations, 22+ years of IT experience</li>
						    <li>Passionate about providing (and receiving) the best possible customer service experience</li>
						    <li>Created Eruv to solve a personal need</li>
						</ul>
						<a href="mailto:adam@twolikeyou.com">adam@twolikeyou.com<div class="liseret-anim"></div></a>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<div id="part-9-person-2" class="part-9-person">
				<div class="infos" data-uk-scrollspy="{cls:'uk-animation-slide-left', topoffset: -100, repeat:false ,delay:400}">
					<div class="infos-content">
						<div class="name">David Lorber</div>
						<div class="status">Founder & Chief Customer Officer</div>
						<ul class="text-1">
						    <li>Ph.D. from the Northwestern University Institute for Neuroscience</li>
						    <li>Global product management, marketing, and sales operations experience</li>
						    <li>Love solving problems – especially involving data</li>
						    <li>Saw a need to target the right content to the right people</li>
						</ul>
						<a href="mailto:david@twolikeyou.com">david@twolikeyou.com<div class="liseret-anim"></div></a>
					</div>
				</div>
				<div class="portrait" data-uk-scrollspy="{cls:'uk-animation-slide-right', topoffset: -100, repeat:false ,delay:400}">
					<div class="portrait-bg"></div>
					<img src="<?php echo $baseUrl .'/images/person-22.png' ?>" width="400" height="488" />
				</div>
				<div class="clear"></div>
			</div>
		</div>

		<div id="part-9-persons-responsive" class="responsive">
			<div class="part-9-person-responsive" id="person-1-responsive">
				<img src="<?php echo $baseUrl .'/images/person-12-responsive.png' ?>" class="portrait" width="100%" height="auto" />
				<div class="infos">
					<div class="name">Adam Furman</div>
					<div class="status">Founder & CEO</div>
					<ul class="text-1">
					    <li>MBA, Rady School of Management, UCSD</li>
					    <li>Leadership of Global IT Operations, 22+ years of IT experience</li>
					    <li>Passionate about providing (and receiving) the best possible customer service experience</li>
					    <li>Created Eruv to solve a personal need</li>
					</ul>
					<a href="mailto:adam@twolikeyou.com">adam@twolikeyou.com<div class="liseret-anim"></div></a>
				</div>
			</div>
			<div class="part-9-person-responsive" id="person-2-responsive">
				<img src="<?php echo $baseUrl .'/images/person-22-responsive.png' ?>" class="portrait" width="100%" height="auto" />
				<div class="infos">
					<div class="name">David Lorber</div>
					<div class="status">Founder & Chief Customer Officer</div>
					<ul class="text-1">
					    <li>Ph.D. from the Northwestern University Institute for Neuroscience</li>
					    <li>Global product management, marketing, and sales operations experience</li>
					    <li>Love solving problems – especially involving data</li>
					    <li>Saw a need to target the right content to the right people</li>
					</ul>
					<a href="mailto:david@twolikeyou.com">david@twolikeyou.com<div class="liseret-anim"></div></a>
				</div>
			</div>
		</div>
	</div>
</div>

<!--
<div id="part-10" class="part">
	<div id="part-10-content">
		<div id="part-10-left">
			<h3>Eruv by two like you</h3>
			<div class="text text-1">
				Combining our complementary expertise, we created Eruv.  Working closely with early adopting 
				congregations around the country, we refined our ideas with feedback and guidance from our customers to organically grow eruv. Today eruv is still very much evolving based on customer driven improvements.
			</div>
		</div>

		<div id="part-10-right">
			<h3>WHy THE NAMe "eruv"</h3>
			<div class="text text-1">
				An ‘eruv,’ is an urban area enclosed by a wire boundary that symbolically extends the private domain of Jewish households into public areas, permitting activities within it that are normally forbidden in public on the Sabbath.  We the take ‘eruv,’ as simply an extension of your congregation that your members keep with them wherever they go.

			</div>
		</div>

		<div class="clear"></div>
	</div>
</div>
-->

<!--
<div id="part-10-bis" class="responsive">
	<div id="part-10-content-responsive" class="large-width">
		<img src="<?php echo $baseUrl .'/images/contact-us.jpg' ?>" id="contact-us-picto" width="80" heigh="56" />
		<div id="contact-text">We’d love your feedback! What’s on your mind?</div>
		<a href="#" id="btn-contact" class="button button-white">Send us a note</a>
	</div>
</div>
-->

<div id="part-11" class="part" data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false ,delay:100}">
	<div id="part-11-content" class="large-width">
		<!-- <div id="subtitle">See for your self</div> -->
		<div id="title" class="desktop">
			Serious About Member Engagment?<br />
			Let’s Talk.
		</div>
		<div id="title" class="responsive">
			Serious About<br />
			Member Engagment?<br />
			Let’s Talk.
		</div>
		<div id="contact">
			<a href="#" class="button button-blue btn-animate" id="request-demo">
				<div class="btn-content-hover">Request a demo</div>
				<div class="btn-content">Request a demo</div>
			</a>
			<a href="mailto:monmail@mail.com" class="button button-grey btn-animate" id="email">
				<div class="btn-content-hover">Email us</div>
				<div class="btn-content">Email us</div>
			</a>
			<div id="separation" class="desktop"></div>
			<div id="phone">
				<div id="text">Call us</div>
				<a href="858 633 3858" id="num">858 633 3858</a>
			</div>
		</div>
	</div>
</div>

<footer data-uk-scrollspy="{cls:'uk-animation-fade', topoffset: -100, repeat:false ,delay:100}">
	<img src="<?php echo $baseUrl .'/images/logo-black.png' ?>" id="logo-footer" width="109" height="35" />
	<ul class="desktop">
	    <li class="product-link menu-link">Product</li>
	    <li class="testimonials-link menu-link">Testimonials</li>
	    <li class="menches-link menu-link">Your Mensches</li>
	</ul>
	<div id="copyright">©2016 Eruv - Designed by <a href="http://www.ouiwill.com/" id="ouiwill_link" target="_blink">Oui Will</a></div>
</footer>